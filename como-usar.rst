Uso e cuidados
==================================================

Vamos delinear os passos para ter uma boa experiência com as leveduras kveik na fabricação de cervejas com técnicas modernas.
Pressupõe-se que o leitor tenha conhecimento das técnicas gerais, em termos práticos e teóricos, como mosturação, fervura, fermentação e maturação.

De forma geral, o uso de uma cultura *kveik* em um lote de cerveja pode ser descrito conforme o seguinte roteiro:

1. Identificação de cultura ou cepa;
2. Averiguação de quantidade disponível:
   Propagação com *starter* caso a quantidade disponível seja insuficiente (e.g. poucas lascas) *ou*
   Separação de quantidade para o mosto;
3. Brassagem e resfriamento para temperatura de inoculação desejada;
4. (*Opcional*) Controle de temperatura;
5. Coleta;
6. Secagem;

Identificação
-------------

Como citado em :doc:`definicoes-historia`, há um catálogo, criado e mantido por Lars Marius Garshol, contendo informações geográficas, técnicas e sensoriais sobre diversas culturas *kveik* e algumas não *kveik*.
Esse catálogo encontra-se aqui_. Vamos aprender a interpretar a organização e as informações desse catálogo.

Inicialmente o catálogo contém informações sobre si mesmo, como qual sua finalidade e um pouco de seu histórico.
Em seguida, há um mapa que relaciona cada cultura do catálogo com a região de onde foi extraída.

.. warning:: Observe que algumas das culturas não foram extraídas da Noruega, como por exemplo a Simonaitis - extraída da Lituânia.
          As culturas *kveik* são **necessariamente** (i.e. por definição) da Noruega. No entanto, o catálogo também contém algumas culturas que são comportamentalmente parecidas com as *kveik*.
          Um termo proposto para culturas que exibem comportamento similar às *kveik* é *landrace*.

Abaixo do mapa, há uma tabela com diversas entradas. Cada entrada é uma cultura registrada e o número de cada cultura é tido como o "padrão".
O que significa ser "padrão"? Toda vez que você ver alguém dizendo "#1 Hornindal" ou "#15 Nornes", por exemplo, essa pessoa está se referindo às culturas conforme catalogadas nessa tabela.

Vamos listar o significado de cada uma das colunas dessa tabela:

- `No`: o número da cultura;
- `Name`: o nome da cultura;
- `Owner`: a pessoa (muitas vezes fazendeiro(a)) que doou a cultura;
- `Place`: o local de onde a cultura foi extraída;
- `NCYC`: o registro da cultura no NCYC (*National Collection of Yeast Cultures*, banco de culturas de leveduras do Reino Unido), caso exista;
- `Pitch`: temperatura, em graus Celsius, de inoculação utilizada pelos donos originais da cultura;
- `Harvest`: diz se a cultura foi registrada como sendo coletada por *krausen*, lama ou ambos;
- `Bacteria`: informa presença ou não de bactéria na cultura, caso sequenciada;
- `POF`: caso a cultura produza fenóis (sempre negativo no caso de culturas *kveik*);
- `Strains`: quantas cepas de *Saccharomyces* a cultura contém;
- `Max ABV`: máximo teor alcoólico obtido com a cultura;

Observe que algumas entradas dessa tabela estão em branco, pelo simples fato de não haver informações disponíveis sobre algumas culturas.

Ao clicar no número de alguma cultura, você é levado ao registro mais detalhado da cultura do respectivo número.
O registro detalhado contém algumas informações adicionais sobre a cultura que valem a pena ser discutidos.

Primeiramente, alguns registros contêm informações mais técnicas sobre as culturas, como temperatura de inoculação e máxima de crescimento, atenuação, tempo no qual a coleta deve ser realizada e espécie das cepas.
Vamos interpretar essas informações com cautela: elas são oriundas da observação do uso em lotes tradicionais de fazenda. Como vimos em :doc:`definicoes-historia`, os lotes tradicionais são **muito diferentes** dos que usualmente fazemos.
As temperaturas de inoculação, por exemplo, levam em conta o volume grande (e consequente retenção de calor) e usualmente estão acima de 30°C justamente para acelerar o metabolismo das leveduras.
A interpretação do tempo de coleta deve ser feito da mesma forma: se uma cultura está listada como sendo coletada 48h após a inoculação, isso não é necessariamente verdade para lotes que fazemos.

Além das informações técnicas, alguns registros contém informações sobre sabor, proveniência e alguns nomes comerciais para as culturas.
Novamente, é necessário atentar-se ao fato de que não necessariamente produtos comercializados com o mesmo nome de uma cultura X contêm as mesmas cepas e micro-organismos da *cultura original* X.

Propagação
----------

Vamos partir do princípio de que você recebeu uma amostra de determinada cultura ou cepa.
As amostras enviadas por cartas são lascas ou flocos finos que constituem cerca de apenas 1 grama. 
Embora essa quantidade seja suficiente para fermentar 10 litros (supondo condições ótimas de armazenamento) :cite:`garsholpreiss2018`, 
não é recomendado utilizar toda a amostra recebida diretamente em uma leva.
Como o processo de compartilhamento pode ser demorado, recomenda-se fazer um *starter* com metade das lascas recebidas, para manter assim um *backup* para ser utilizado caso algum problema aconteça.

*Starter*
   *Starter* (algo como "iniciador" em português) é uma pequena quantidade de mosto em que a levedura é inoculada para se propagar. 
   A finalidade do *starter* é obter uma quantidade suficiente de leveduras para fermentar um lote arbitrário de mosto.
   
Embora as *kveik* sejam bem indulgentes em relação ao *pitch rate* (taxa de inoculação, a densidade de leveduras no mosto), tendem a ser mais exigentes em relação à densidade do *starter* e, consequentemente, quantidade de nutrientes.
Tradicionalmente, os fazendeiros pegavam uma pequena quantidade do mosto no início da brassagem e inoculavam a cultura *kveik*, efetivamente fazendo um *starter* de algumas horas.
É comum fazermos *starters* com densidade de 1.040 para leveduras convencionais, mas as *kveik* estão acostumadas a *starters* com densidade entre 1.080 e 1.120. 
No entanto, isso não significa que não irão se propagar em um *starter* convencional, apenas que se propagarão mais lentamente.

.. tip:: Para fazer um *starter* de forma barata e eficaz, basta fazer uma "mini-mostura" a 65°C de 200 gramas de malte base por litro.

Inoculação e fermentação
------------------------

Coleta e armazenamento
----------------------

.. _aqui: http://www.garshol.priv.no/download/farmhouse/kveik.html