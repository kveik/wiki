.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bem-vindo à Kveik Wiki!
=======================

Esta wiki é uma iniciativa colaborativa com a finalidade de agregar conhecimento sobre técnicas tradicionais e atuais para utilizar as leveduras norueguesas conhecidas como kveik.

Conteúdo:
=========

.. toctree::
   :maxdepth: 2
   
   definicoes-historia
   como-usar
   faq

Contribua com este projeto
==========================

* https://gitlab.com/kveik/wiki

Agradecimentos
==============

Agradecemos aos pesquisadores Odd Nordland, Lars Marius Garshol, Richard Preiss, Rodrigo Borges de Azevedo, Dan Pixley (em nome da MTF Wiki) por contribuírem direta ou indiretamente na construção desta wiki.
