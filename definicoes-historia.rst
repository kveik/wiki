Definições e história
==================================================

Termos
------

Vamos explorar alguns termos e fragmentos históricos das kveik!
Começaremos definindo exatamente o que é e o que significa "kveik".

*Kveik*
   *Kveik*, pronunciado como *bike* ou também como *rake* em inglês, significa "levedura" em um dialeto norueguês da parte ocidental da Noruega :cite:`veka2001norsk`.
   
.. note:: Kveik **não** é um estilo de cerveja, portanto é incorreto dizer que você irá "fazer uma kveik". Fazemos cervejas *com* kveik.
   
Observe que é um pouco redundante dizer "levedura kveik" - isso significa, estritamente, algo como "levedura levedura" - , 
mas atualmente kveik é utilizado como um adjetivo que se refere a uma classe de leveduras originárias da Noruega que são tradicionalmente utilizadas por fazendeiros para fazer cerveja.

Algumas características das kveik incluem :cite:`preissetal2018`:

* Capacidade de fermentar em altas temperaturas (acima de 30°C), com rápida atenuação;
* Produzem perfil sensorial predominantemente composto por ésteres, ausência de fenóis (são POF-);
* Não são capazes de metabolizar maltotriose ou dextrinas (são STA1-) e podem terminar a fermentação em gravidades relativamente altas (em torno de 1.020);
* Podem ser coletadas e são adaptadas a um processo de secagem rudimentarm, mantendo-se viáveis por anos após secas;
* Estão acostumadas a ambientes com muitos nutrientes, i.e., mostos com densidade inicial acima de 1.080;
* Não são tradicionalmente utilizadas isoladas, mas sim dentro de *culturas*.

Cultura
   No contexto das kveik, uma cultura é um grupo de micro-organismos com uma ou mais cepas do gênero *Saccharomyces* utilizadas por um fazendeiro norueguês de determinada região.
   Algumas culturas possuem micro-organismos que não do gênero *Saccharomyces*, como *Lactobacillus* e *Gluconobacter*.

Lars Marius Garshol é responsável por manter um catálogo, chamado `Farmhouse yeast registry`_, com várias informações acerca das culturas kveik.
Leia mais sobre esse catálogo em :doc:`como-usar`.

.. figure:: diversity.jpg
   :alt: Exemplos de culturas kveik.
   :scale: 40%

   Exemplos da diversidade de culturas e formas de armazenar (Adam Cogswell, 2019).

O fato de uma cultura potencialmente possuir mais de uma cepa de *Saccharomyces* pode resultar em uma distribuição populacional não igualitária: 
é possível e provável que cada cepa não esteja presente em mesma quantidade em determinado lote de cerveja.
Outra consequência da multiplicidade de cepas é que diferentes laboratórios podem vender cepas isoladas de culturas com características muito diferentes.

Os micro-organismos que não são *Saccharomyces* podem contribuir de formas imprevisíveis e inusitadas no comportamento das culturas. Alguns exemplos:

* A cultura #5 Hornindal, de Terje Raftevold, é um exemplo de cultura que contém bactérias e produz um sabor descrito como "caramelado e de cogumelo".
  As cepas/culturas purificadas atualmente comercializadas por laboratórios e não possuem esse descritivo de sabor, como a `OYL-091`_. Isso sugere que as bactérias têm um papel muito maior do que apenas acidificar o mosto.
* A cepa isolada da cultura #1 Sigmund, de Sigmund Gjernes, pela Omega Yeast Labs (`OYL-061`_) não possui grande capacidade de floculação, embora a cultura original tenha sido reportada como altamente floculante em decorrência de um fenômeno descrito como "cofloculação" :cite:`10.1371/journal.pone.0136249`.

.. warning:: Sempre que compartilhar kveik com alguém, além de especificar qual a cultura de origem, é necessário especificar:
   
   * se é a cultura original não purificada *ou*
   * se é purificada e contém diversas cepas *ou*
   * se é apenas uma cepa isolada.

História
--------

Sabemos, através da legislação nórdica histórica Gulating :cite:`larson2008earliest`, que os fazendeiros do local que hoje é chamado de Noruega faziam cerveja desde o tempo dos vikings (entre 800 e 1066).
De fato, a legislação obrigava que todos fazendeiros utilizassem parte de seus grãos para fabricar cerveja. 
Evidentemente, as leveduras utilizadas pelos fazendeiros de então não são o que classificaríamos como kveik atualmente, mas a prática de fazer cerveja aliada ao desenvolvimento de técnicas de reutilização contribuiram na domesticação das leveduras.

Sobre o tópico de reutilização de leveduras, há registros de que desde 1621 (e potencialmente antes) as kveik eram coletadas por meio de um bloco de madeira - *kveikstokk* ("levedura" + "bloco de madeira").
O kveikstokk era imerso na cerveja durante alta fermentação ou na lama e armazenado seco por períodos longos de tempo, cerca de um ano ou mais.
Embora o kveikstokk tenha sido o método mais notável para coletar leveduras, ele certamente não é o único: há também o anel de madeira *gjærkrans* ("levedura" + "guirlanda"), diversas superfícies para secagem como casca de árvores, palha, galhos de zimbro, entre outros :cite:`nordland1969brewing`.

.. note:: As culturas de cada fazenda eram passadas de geração em geração, bem como o ensino das técnicas utilizadas.
   Quando um fazendeiro perdia sua cultura por contaminação, ele simplesmente obtia mais de seus vizinhos - quem tinha levedura, compartilhava sem cobrar.

.. figure:: kveik-drying.png
   :alt: Exemplos de aparatos para secagem de kveik.

   Exemplos de aparatos para secagem de kveik e modelos de kveikstokk :cite:`nordland1969brewing`.

Algo único sobre a cerveja tradicional feita pelos fazendeiros noruegueses é que cada fazendeiro fazia apenas um "estilo" com malte de cevada e os ingredientes que tinha a disposição, denominado genericamente de *maltøl*.
Obviamente, cada local tinha suas peculiaridades de ingredientes, portanto o nome mais genérico é alterado para se adequar e se referir à cerveja de um local específico - por exemplo, a cerveja produzida em Voss é chamada de Vossaøl :cite:`garsholpreiss2018`.

Algumas características comuns aos mostos tradicionais noruegueses incluem:

* densidade inicial bastante alta (cerca de 1.080 ou mais) obtido de um processo longo de mostura no limite superior de conversão;
* uso de `baga de zimbro`_ como principal ingrediente de amargor e lúpulo como agente antibacterial;
* duas opções gerais de fervura: intensa em um fogo aberto *ou* inexistente (nesse caso, a cerveja fica classificada como *raw ale*);
* sem medições (e.g., densidade inicial, IBUs);
* os fazendeiros se concentravam em fazer lotes grandes poucas vezes ao ano.

As kveik eram inoculadas no mosto a uma temperatura entre 28°C e 40°C, através do anel ou bloco de madeira, por exemplo. 
Esperava-se que a fermentação terminasse em 1-2 dias ou até menos, resultando em uma cerveja doce e alcoólica :cite:`garsholpreiss2018,nordland1969brewing`.
Após o término da fermentação, a cerveja era transferida do recipiente de fermentação para barril ou tonel em um evento chamado de *oppskåke*.
Durante o oppskåke, o fazendeiro e seus vizinhos convidados se reuniam para beber parte da cerveja produzida, *sem carbonatação e em temperatura ambiente*, como uma espécie de festa de comemoração.

Atualmente os métodos tradicionais de fabricação de cerveja na Noruega estão ameaçados de extinção e foram parcialmente adaptados com algumas invenções modernas, como uso de alguns recipientes de plástico.
Não é comum encontrar cerveja produzida dessa forma nos supermercados noruegueses, é necessário visitar locais específicos para comprar ou obter as bebidas realmente tradicionais.

Referências
-----------

.. bibliography:: references.bib

Material adicional
------------------

Lars M. Garshol - "Kveik" - what does it mean?": http://www.garshol.priv.no/blog/380.html

Lars M. Garshol - Norwegian farmhouse ale styles: http://www.garshol.priv.no/blog/366.html

Lars M. Garshol - Brewing Raw Ale in Hornindal: http://www.garshol.priv.no/blog/342.html

Lars M. Garshol - Oppskåke: http://www.garshol.priv.no/blog/344.html

Milk The Funk - Kveik: http://www.milkthefunk.com/wiki/Kveik

.. _OYL-091: https://omegayeast.com/yeast/norwegian-ales/hornindal-kveik
.. _OYL-061: https://omegayeast.com/yeast/norwegian-ales/voss-kveik
.. _Farmhouse yeast registry: http://www.garshol.priv.no/download/farmhouse/kveik.html
.. _`baga de zimbro`: https://pt.wikipedia.org/wiki/Baga_de_zimbro